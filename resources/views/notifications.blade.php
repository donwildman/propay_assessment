
@if (session('success'))
<div class="row">
	<div class="col-sm-12">
		<div class="alert alert-success alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <strong>Success:</strong> {{ session('success') }}
		</div>
	</div>
</div>

@endif

@if (session('notice'))
<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="alert alert-info alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <strong>Notice:</strong> {{ session('notice') }}
		</div>
	</div>
</div>

@endif

@if (session('error'))
<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="alert alert-danger alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <strong>Error:</strong> 

			{{ session('error') }}

		  
		</div>
	</div>
</div>

@endif

@if (session('warning'))
<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="alert alert-warning alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <strong>Warning:</strong><br/> {{ session('warning') }}
		</div>
	</div>
</div>

@endif

@if (session('info'))
<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="alert alert-info alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <strong>FYI:</strong> {{ session('info') }}
		</div>
	</div>
</div>

@endif
