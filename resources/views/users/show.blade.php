@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>

                <div class="panel-body">

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('update', ['id' => $user['id']]) }}">
                        {{ csrf_field() }}


                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $user['name'] }}" >

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                            <label for="surname" class="col-md-4 control-label">Surame</label>

                            <div class="col-md-6">
                                <input id="surname" type="text" class="form-control" name="surname" value="{{ $user['surname'] }}" >

                                @if ($errors->has('surname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('id_number') ? ' has-error' : '' }}">
                            <label for="id_number" class="col-md-4 control-label">ID No.</label>

                            <div class="col-md-6">
                                <input id="id_number" type="text" class="form-control" name="id_number" value="{{  $user['id_number'] }}" >

                                @if ($errors->has('id_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="mobile" class="col-md-4 control-label">Mobile No.</label>

                            <div class="col-md-6">
                                <input id="mobile" type="text" class="form-control" name="mobile" value="{{ $user['mobile'] }}" >

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : '' }}">
                            <label for="birth_date" class="col-md-4 control-label">Birth Date</label>

                            <div class="col-md-6">
                                <input id="birth_date" type="text" class="form-control" name="birth_date" value="{{  $user['birth_date'] }}" >

                                @if ($errors->has('birth_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('birth_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('language') ? ' has-error' : '' }}">
                            <label for="language" class="col-md-4 control-label">Language</label>

                            <div class="col-md-6 text-left">
                                <div class="radio">
                                    <label>
                                        <input  type="radio" value="English" name="language" id="language1" @if($user['language'] == 'English') checked="checked" @endif> English
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input  type="radio" value="Afrikaans" name="language" id="language2" @if($user['language'] == 'Afrikaans') checked="checked" @endif> Afrikaans
                                    </label>
                                </div>
                                @if ($errors->has('language'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('language') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('interests') ? ' has-error' : '' }}">
                            <label for="interests" class="col-md-4 control-label">Interests</label>

                            <div class="col-md-6 text-left">
                                <div class="checkbox">
                                    <label>
                                        <input  type="checkbox" value="Computers" name="interests[]" @if(in_array('Computers',$user['interests'])) checked="checked" @endif > Computers
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input  type="checkbox" value="Technology" name="interests[]" @if(in_array('Technology',$user['interests'])) checked="checked" @endif> Technology
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input  type="checkbox" value="Travel" name="interests[]" @if(in_array('Travel',$user['interests'])) checked="checked" @endif> Travel
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input  type="checkbox" value="Religion" name="interests[]" @if(in_array('Religion',$user['interests'])) checked="checked" @endif> Religion
                                    </label>
                                </div>

                                @if ($errors->has('interests'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('interests') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $( "#birth_date" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd'
        });
    });

</script>
@endsection
