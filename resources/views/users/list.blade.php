@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>

                <div class="panel-body">

                    <ul class="list-group">
                        @foreach($users as $user)
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-8">
                                    {{ $user->surname }}, {{ $user->name }}
                                </div>
                                <div class="col-sm-4 text-right">

                                    <a href="{{ route('edit', ['id' => $user->id]) }}" class="btn btn-sm btn-info " title="Edit User" ><i class="fa fa-pencil-square"></i></a>
                                    <a href="{{ route('delete', ['id' => $user->id]) }}" class="btn btn-sm btn-danger" title="Delete User" ><i class="fa fa-trash"></i></a>
                                </div>
                            </div>


                        </li>
                        @endforeach
                    </ul>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
