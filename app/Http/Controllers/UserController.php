<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['users'] = \App\User::orderBy('surname')->orderBy('name')->get();
        return view('users.list', $data);
    }

    public function getUser(Request $request,$id)
    {
        $user = User::find($id);
        if(empty($user))
        {
            Session::flash('error', 'Invalid User!');

            return redirect('users');
        }

        $data['user'] = $user->toArray();
        $interests = json_decode($user->interests, true);
        $data['user']['interests'] = $interests;

        return view('users.show', $data);
    }

    public function postUser(Request $request, $id)
    {

        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'id_number' => 'required',
            'mobile' => 'required',
            'birth_date' => 'required|date',
            'language' => 'required',
            'interests' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect(URL::previous())
                ->withErrors($validator)
                ->withInput();
        }




        $user = User::find($id);
        if(empty($user))
        {
            Session::flash('error', 'Invalid User!');
            $data['users'] = \App\User::orderBy('surname')->orderBy('name')->get();
            return view('users.list', $data);
        }

        $user->name = $request->input('name');
        $user->surname = $request->input('surname');
        $user->id_number = $request->input('id_number');
        $user->mobile = $request->input('mobile');
        $user->language = $request->input('language');
        $user->interests = json_encode($request->input('interests'));

        if($user->save()){
            Session::flash('success', 'User updated!');
            return redirect('/users');
        } else {
            Session::flash('error', 'User not updated!');
            return view('users.edit')->withInput();
        }



    }


    public function deleteUser($id)
    {
        $user = User::find($id);
        if(!empty($user))
        {
            $user->delete();
            Session::flash('success', 'User deleted!');
            return redirect('/users');
        } else {
            Session::flash('error', 'Invalid User!');
            return redirect('/users');
        }


    }
}
