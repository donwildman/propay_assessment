# README #

### How do I get set up? ###

* Setup local virtual host with the /public folder as the root of the website
* Clone this repo
* Create .env file with your local database credentials and hosting info
* Run 'composer update'
* Run 'php artisan migrate'
* Run 'php artisan db:seed' to set up the admin user (admin@propay.com:propayadmin)
* Point your browser to the local host.


