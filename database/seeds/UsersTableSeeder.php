<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insertGetId([
            'email' => 'admin@propay.com',
            'password' => bcrypt('propayadmin'),
            'name' => 'Admin',
            'surname' => 'ProPay',
            'id_number' => '1234567890123',
            'mobile' => '0831234567',
            'birth_date' => '1995-03-01',
            'language' => 'English',
            'interests' => json_encode(["Computers","Technology"]),
            'group' => 'Admin'
        ]);


    }
}
